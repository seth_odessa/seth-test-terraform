variable "region" {
  type    = string
  default = "us-east-1"
}

variable "ami" {
  type    = string
  default = "ami-02e136e904f3da870"
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "key_name" {
  type    = string
  default = "WA-KeyPair"
}

variable "security_groups" {
  type = string
  default = "sg-0dbf308b6496b96a3"
  
}
variable "subnet_id" {
  type    = string
  default = "subnet-0d9ce85bb1e408de5"
}
variable "vpc_id" {
  type = string
  default = "vpc-008fd2062d8d610b6"
}

variable "volume_size" {
  type    = number
  default = 50

}

variable "availability_zone" {
  type    = string
  default = "us-east-1a"
}

variable "vpc_security_group_ids" {
  type = list(string)
  default = ["sg-0dbf308b6496b96a3"]
}

variable "user_data" {
  type    = string
  default = "./user_data.sh"
}

variable "aws_access_key" {
  type = string
  default = "AKIARTGYDGMHJF3ZMXPX"
}

variable "aws_secret_key" {
  type = string
  default = "5cEOrsp85/f44U7HB0PNT/Lwq+gzh+Wfqdxrz7sC"  
}

variable "environment" {
  type = string
  default = "WA-KeyPair"
}

variable "asg_group_id" {
  type = string
  default = "WebAge-App"
}

variable "tg_port" {
  type = string
  default = "80"
}

variable "protocol" {
  type = string
  default = "HTTP"
}

variable "alb_name" {
  type = string
  default = "WebAge-App"
}